// P1642
//      Full
#include<cstddef>
#include<version>
#include<limits>
#include<climits>
#include<cfloat>
#include<cstdint>
#include<new>
#include<typeinfo>
#include<type_traits>
#include<source_location>
#include<exception>
#include<initializer_list>
#include<compare>
#include<coroutine>
#include<cstdarg>
#include<concepts>
#include<utility>
#include<tuple>
#include<ratio>
#include<bit>

//      Modified
#include<cstdlib>
#include<memory>
#include<functional>
#include<iterator>
#include<ranges>
#include<atomic>

// P2198
#ifdef P2198

#if _GLIBCXX_USE_ALLOCATOR_NEW
#warning It is defined
#endif
#ifndef    __cpp_lib_addressof_constexpr
#error    __cpp_lib_addressof_constexpr
#endif
#ifndef    __cpp_lib_allocator_traits_is_always_equal
#error    __cpp_lib_allocator_traits_is_always_equal
#endif

#ifndef    __cpp_lib_apply
#error    __cpp_lib_apply
#endif
#ifndef    __cpp_lib_as_const
#error    __cpp_lib_as_const
#endif
#ifndef    __cpp_lib_assume_aligned
#error    __cpp_lib_assume_aligned
#endif
#ifndef    __cpp_lib_atomic_flag_test
#error    __cpp_lib_atomic_flag_test
#endif
#ifndef    __cpp_lib_atomic_float
#error    __cpp_lib_atomic_float
#endif
#ifndef    __cpp_lib_atomic_is_always_lock_free
#error    __cpp_lib_atomic_is_always_lock_free
#endif
#ifndef    __cpp_lib_atomic_ref
#error    __cpp_lib_atomic_ref
#endif
#ifndef    __cpp_lib_atomic_value_initialization
#error    __cpp_lib_atomic_value_initialization
#endif
#if defined _GLIBCXX_HAS_GTHREADS || defined _GLIBCXX_HAVE_LINUX_FUTEX
#ifndef    __cpp_lib_atomic_wait
#error    __cpp_lib_atomic_wait
#endif
#endif // GTHERADS || LINUX_FUTEX
#ifndef __GNUC__ // Commented out until GCC supports bind_back in 23 mode
#ifndef    __cpp_lib_bind_back
#error    __cpp_lib_bind_back
#endif
#endif
#ifndef    __cpp_lib_bind_front
#error    __cpp_lib_bind_front
#endif
#ifndef    __cpp_lib_bit_cast
#error    __cpp_lib_bit_cast
#endif
#ifndef    __cpp_lib_bitops
#error    __cpp_lib_bitops
#endif
#ifndef    __cpp_lib_bool_constant
#error    __cpp_lib_bool_constant
#endif
#ifndef    __cpp_lib_bounded_array_traits
#error    __cpp_lib_bounded_array_traits
#endif
#ifndef    __cpp_lib_byte
#error    __cpp_lib_byte
#endif
#ifndef    __cpp_lib_byteswap
#error    __cpp_lib_byteswap
#endif
#ifndef    __cpp_lib_char8_t
#error    __cpp_lib_char8_t
#endif
#ifndef    __cpp_lib_concepts
#error    __cpp_lib_concepts
#endif
#ifndef    __cpp_lib_constexpr_functional
#error    __cpp_lib_constexpr_functional
#endif
#ifndef    __cpp_lib_constexpr_iterator
#error    __cpp_lib_constexpr_iterator
#endif
#ifndef    __cpp_lib_constexpr_memory
#error    __cpp_lib_constexpr_memory
#endif
#ifndef    __cpp_lib_constexpr_tuple
#error    __cpp_lib_constexpr_tuple
#endif
#ifndef    __cpp_lib_constexpr_typeinfo
#error    __cpp_lib_constexpr_typeinfo
#endif
#ifndef    __cpp_lib_constexpr_utility
#error    __cpp_lib_constexpr_utility
#endif
#ifndef    __cpp_lib_destroying_delete
#error    __cpp_lib_destroying_delete
#endif
#ifndef    __cpp_lib_endian
#error    __cpp_lib_endian
#endif
#ifndef    __cpp_lib_exchange_function
#error    __cpp_lib_exchange_function
#endif
#ifndef __GNUC__ // Comment out until GCC supports forward_like
#ifndef    __cpp_lib_forward_like
#error    __cpp_lib_forward_like
#endif
#endif
#ifndef    __cpp_lib_hardware_interference_size
#error    __cpp_lib_hardware_interference_size
#endif
#ifndef    __cpp_lib_has_unique_object_representations
#error    __cpp_lib_has_unique_object_representations
#endif
#ifndef    __cpp_lib_int_pow2
#error    __cpp_lib_int_pow2
#endif
#ifndef    __cpp_lib_integer_sequence
#error    __cpp_lib_integer_sequence
#endif
#ifndef    __cpp_lib_integral_constant_callable
#error    __cpp_lib_integral_constant_callable
#endif
#ifndef    __cpp_lib_invoke
#error    __cpp_lib_invoke
#endif
#ifndef    __cpp_lib_invoke_r
#error    __cpp_lib_invoke_r
#endif
#ifndef    __cpp_lib_is_aggregate
#error    __cpp_lib_is_aggregate
#endif
#ifndef    __cpp_lib_is_constant_evaluated
#error    __cpp_lib_is_constant_evaluated
#endif
#ifndef    __cpp_lib_is_final
#error    __cpp_lib_is_final
#endif
#ifndef    __cpp_lib_is_invocable
#error    __cpp_lib_is_invocable
#endif
#ifndef    __cpp_lib_is_layout_compatible
#error    __cpp_lib_is_layout_compatible
#endif
#ifndef    __cpp_lib_is_nothrow_convertible
#error    __cpp_lib_is_nothrow_convertible
#endif
#ifndef    __cpp_lib_is_null_pointer
#error    __cpp_lib_is_null_pointer
#endif
#ifndef    __cpp_lib_is_pointer_interconvertible
#error    __cpp_lib_is_pointer_interconvertible
#endif
#ifndef    __cpp_lib_is_scoped_enum
#error    __cpp_lib_is_scoped_enum
#endif
#ifndef    __cpp_lib_is_swappable
#error    __cpp_lib_is_swappable
#endif
#ifndef    __cpp_lib_launder
#error    __cpp_lib_launder
#endif
#ifndef    __cpp_lib_logical_traits
#error    __cpp_lib_logical_traits
#endif
#ifndef    __cpp_lib_make_from_tuple
#error    __cpp_lib_make_from_tuple
#endif
#ifndef    __cpp_lib_make_reverse_iterator
#error    __cpp_lib_make_reverse_iterator
#endif
#ifndef __GNUC__ // Comment out until GCC supports standard library modules
#ifndef    __cpp_lib_modules
#error    __cpp_lib_modules
#endif
#endif
#ifndef    __cpp_lib_move_iterator_concept
#error    __cpp_lib_move_iterator_concept
#endif
#ifndef    __cpp_lib_nonmember_container_access
#error    __cpp_lib_nonmember_container_access
#endif
#ifndef    __cpp_lib_not_fn
#error    __cpp_lib_not_fn
#endif
#ifndef    __cpp_lib_null_iterators
#error    __cpp_lib_null_iterators
#endif
#ifndef    __cpp_lib_ranges_as_const
#error    __cpp_lib_ranges_as_const
#endif
#ifndef    __cpp_lib_ranges_as_rvalue
#error    __cpp_lib_ranges_as_rvalue
#endif
#ifndef    __cpp_lib_ranges_cartesian_product
#error    __cpp_lib_ranges_cartesian_product
#endif
#ifndef    __cpp_lib_ranges_chunk
#error    __cpp_lib_ranges_chunk
#endif
#ifndef    __cpp_lib_ranges_chunk_by
#error    __cpp_lib_ranges_chunk_by
#endif
#ifndef    __cpp_lib_ranges_join_with
#error    __cpp_lib_ranges_join_with
#endif
#ifndef    __cpp_lib_ranges_repeat
#error    __cpp_lib_ranges_repeat
#endif
#ifndef    __cpp_lib_ranges_slide
#error    __cpp_lib_ranges_slide
#endif
#ifndef    __cpp_lib_ranges_stride
#error    __cpp_lib_ranges_stride
#endif
#ifndef __GNUC__ // Comment out until GCC gets ranges_to_container
#ifndef    __cpp_lib_ranges_to_container
#error    __cpp_lib_ranges_to_container
#endif
#endif
#ifndef    __cpp_lib_ranges_zip
#error    __cpp_lib_ranges_zip
#endif
#ifndef    __cpp_lib_reference_from_temporary
#error    __cpp_lib_reference_from_temporary
#endif
#ifndef    __cpp_lib_remove_cvref
#error    __cpp_lib_remove_cvref
#endif
#ifndef    __cpp_lib_result_of_sfinae
#error    __cpp_lib_result_of_sfinae
#endif
#ifndef    __cpp_lib_source_location
#error    __cpp_lib_source_location
#endif
#ifndef    __cpp_lib_ssize
#error    __cpp_lib_ssize
#endif
#ifndef __GNUC__ // Comment out until GCC gets start lifetime as  (soon I hope)
#ifndef    __cpp_lib_start_lifetime_as
#error    __cpp_lib_start_lifetime_as
#endif
#endif
#ifndef    __cpp_lib_three_way_comparison
#error    __cpp_lib_three_way_comparison
#endif
#ifndef    __cpp_lib_to_address
#error    __cpp_lib_to_address
#endif
#ifndef    __cpp_lib_to_underlying
#error    __cpp_lib_to_underlying
#endif
#ifndef    __cpp_lib_transformation_trait_aliases
#error    __cpp_lib_transformation_trait_aliases
#endif
#ifndef    __cpp_lib_transparent_operators
#error    __cpp_lib_transparent_operators
#endif
#ifndef    __cpp_lib_tuple_element_t
#error    __cpp_lib_tuple_element_t
#endif
#ifndef    __cpp_lib_tuples_by_type
#error    __cpp_lib_tuples_by_type
#endif
#ifndef    __cpp_lib_type_identity
#error    __cpp_lib_type_identity
#endif
#ifndef    __cpp_lib_type_trait_variable_templates
#error    __cpp_lib_type_trait_variable_templates
#endif
#ifndef    __cpp_lib_uncaught_exceptions
#error    __cpp_lib_uncaught_exceptions
#endif
#ifndef    __cpp_lib_unreachable
#error    __cpp_lib_unreachable
#endif
#ifndef    __cpp_lib_unwrap_ref
#error    __cpp_lib_unwrap_ref
#endif
#ifndef    __cpp_lib_void_t
#error    __cpp_lib_void_t
#endif

// in version
#ifndef __cpp_lib_freestanding_feature_test_macros
#warning __cpp_lib_freestanding_feature_test_macros
#endif

#ifndef __cpp_lib_freestanding_utility
#warning __cpp_lib_freestanding_utility
#endif
#ifndef __cpp_lib_freestanding_tuple
#warning __cpp_lib_freestanding_tuple
#endif
#ifndef __cpp_lib_freestanding_ratio
#warning __cpp_lib_freestanding_ratio
#endif
#ifndef __cpp_lib_freestanding_memory
#warning __cpp_lib_freestanding_memory
#endif
#ifndef __cpp_lib_freestanding_functional
#warning __cpp_lib_freestanding_functional
#endif
#ifndef __cpp_lib_freestanding_iterator
#warning __cpp_lib_freestanding_iterator
#endif
#ifndef __cpp_lib_freestanding_ranges
#warning __cpp_lib_freestanding_ranges
#endif

#endif // P2198

#ifdef P2338
#include<cerrno>
#include<system_error>
#include<charconv>
#include<string>
#include<cstring>
#if __STDC_WCHAR_H_FREESTANDING_LIBRARY__ 
#include<cwchar>
#endif
#include<cmath>
#endif // P2338

#ifdef P2407
#include<array>
#include<variant>
#include<optional>
#include<string_view>
#include<algorithm> // Only covers fill_n and swap_ranges
#endif // P2407

#ifdef P2833
#include<expected>

#ifndef __cpp_lib_freestanding_expected
#warning __cpp_lib_freestanding_expected
#endif

#include<span>
#ifdef __cpp_lib_mdspan
#include<mdspan>
#endif
// Also touches memory, but that's in already
#endif // P2833

#ifdef P2976
// Must also include P2338
#include <algorithm> // Already started in P2407
#include <numeric>
#include <random>

#ifndef __cpp_lib_freestanding_numeric
#warning __cpp_lib_freestanding_numeric
#endif
#ifndef __cpp_lib_freestanding_random
#warning __cpp_lib_freestanding_random
#endif

#endif

// 2.1. <cstdlib>, <charconv>, <cmath>, <cinttypes>, <cstring>, <cwchar>, and char_traits
#ifdef P2268_2_1 
#warn "Please test for P2338 instead"
// cstdlib is in p1642
#include<charconv>
#include<cinttypes>
#include<cmath>
#include<cstring>
#include<cwchar>
#include<csetjmp> // included from p0829 test suite
#endif

// 2.2. [diagnostics], <algorithm>, <numeric>, lock_guard, unique_lock, <span>
#ifdef P2268_2_2
#warn "Please test for P2976 instead"
// #include<cerrno> handled by P2338
// #include<system_error> handled by P2338
#include<algorithm>
#include<numeric>
// #include<span> handled by P2833
#endif 
 
#ifdef P2268_2_3
#warn "Please test for P2407 instead"
#include<array>
#include<string> // Needed because of string_view
#include<string_view>
#include<variant>
#include<optional>
#include<bitset>
#endif


#ifdef P2268_2_4
#include<random>
#endif

#ifdef P2268_2_5
#include<chrono>
#endif


#ifdef CONTRACTS_ARE_VOTED_OUT
#include<contracts>
#endif

template < typename T >
concept valued = requires( T type )
{
	type.value();
};

template < typename T >
concept valueless = !valued<T>;


int main()
{
	// auto p = new int; // fails since new is declared but not defined
#ifdef P2338
	char buff[32];
	std::to_chars_result tcr = std::to_chars(std::begin(buff), std::end(buff), 42 );
	int result{};
	std::from_chars_result fcr = std::from_chars(std::begin(buff), tcr.ptr, result);
#endif
#ifdef P2407
	char fillBuff[32];
	std::fill_n(std::begin(fillBuff), 32, '\x32');
	std::swap_ranges(fillBuff, fillBuff + 16, fillBuff + 16);
#endif
#ifdef P2833
	std::expected<int,int> expectation{};
	static_assert( valueless<std::expected<int,int>> );
#endif
#ifdef P2976
	char genBuff[32];
	using namespace std;
	uint_fast32_t random_device = 42;
	auto twister = minstd_rand( random_device ); 
	auto ran = uniform_int_distribution(1,20);
	generate(begin(genBuff), end(genBuff), [&](){return ran(twister);});
#endif // P2976
}

#if _GLIBCXX_HOSTED
#warn This test is supposed to be run with -ffreestanding
#endif

#ifdef P2976
	namespace std
	{
		// int stable_sort; // GCC has a freestanding implementaion of stable sort
		int stable_partition;
		// int inplace_merge; // GCC has a freestanding implementation of inplace_merge
	};
#endif
